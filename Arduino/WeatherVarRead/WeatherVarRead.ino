#include <ArduinoJson.h>
#include <BH1750FVI.h>
#include <DHT.h>
#include <DHT_U.h>
#include <Adafruit_Sensor.h>

/////////////////////////////////////
// DONT FORGET ADD LIBRARIES ABOVE //
/////////////////////////////////////

// SERIAL PROTOCOL 
const byte ARDUINO_OK = 1;
const byte PYTHON_OK = 2;

// ARDUINO PINS
const int DHT11_PIN = 7;
const int LED_PIN = 13;

// OTHER CONSTANTS
const int BLINK_COUNT = 1;
const int BLINK_MS = 500;
const int DHTTYPE = DHT11;
const int SAMPLE_RATE = 500;

// SENSORS AND LED
DHT dht(DHT11_PIN, DHTTYPE);
BH1750FVI lightSensor(BH1750FVI::k_DevModeContLowRes);

// VARIABLES
static float temperature = -1.0;
static float humidity = -1.0;
static int light = -1;
static int arduinoReady = false;


void setup() {
  Serial.begin(9600);

  // Wait until serial port connected
  while(!Serial) { delay(SAMPLE_RATE); }
  
  // Set Pin 13 (LED) as an Output
  pinMode(LED_PIN, OUTPUT);
  
  // Initialize LightSensor and DHTSensor instance
  lightSensor.begin();
  dht.begin();

  // Arduino is now ready
  arduinoReady = true;
}

void loop() {
  // Time between checks
  delay(SAMPLE_RATE);
  
  // All loop tasks
  checkSerialInput();
}


/**
 * PROTOCOL COMMUNICATIONS
 */

void serialFlush(){
  arduinoReady = false;
  while(Serial.available() > 0) {
    char t = Serial.read();
  }
}

void sendReadyMessageToPython() {
  if (arduinoReady) {
    Serial.write(ARDUINO_OK);
  }
}

void checkSerialInput() {
  // No data received by Serial Port
  if (Serial.available() <= 0) {
    // ledBlink(BLINK_COUNT, BLINK_MS); // Blink slow
    sendReadyMessageToPython();
  }
  
  // Data received by Serial Port
  else {
    // ledBlink(BLINK_COUNT*2, BLINK_MS/5); // Blink faster
    if (Serial.read() == PYTHON_OK) {
      // ledBlink(BLINK_COUNT*2, BLINK_MS/10); // Blink fastest as possible
      serialFlush();
      sendDataToPython();
    }
    
    /** If we need to reset arduino state (ready after communication failure) **//**
    if (Serial.read() == PYTHON_STATE) {
    serialFlush();
    arduinoReady = true;
    }*/
  }
}


/**
 * READ AND RESPONSE RELATED FUNCTIONS
 */

void sendDataToPython() {
  readDHT11();
  readBH1750();
  response();
}

void readDHT11() {
  temperature = dht.readTemperature();
  humidity = dht.readHumidity();

  if (isnan(humidity) || isnan(temperature)) {
    // Failed to read from DHT sensor!
    temperature = -999.0;
    humidity = -999.0;
  }
}

void readBH1750() {
  light = lightSensor.GetLightIntensity();

  if (isnan(light)) {
    // Failed to read from BH1750 sensor!
    light = -999;
  }
}

void response() {
  // Create JSON object (document)
  const size_t capacity = JSON_OBJECT_SIZE(3);
  DynamicJsonDocument doc(capacity);  
  
  // Set variables
  doc["temperature"] = temperature;
  doc["humidity"] = humidity;
  doc["light"] = light;  
  
  // Print JSON in string format to be received in Python
  Serial.println(serializeJson(doc, Serial));

  //Wait until Python have received all data correctly...
  while (Serial.read() != PYTHON_OK) {}
  
  //...then arduino is ready to send more data
  arduinoReady = true;
}


/**
 * LED RELATED FUNCTIONS
 */

void ledOn() {
  digitalWrite(LED_PIN, HIGH);
}

void ledOff() {
  digitalWrite(LED_PIN, LOW);
}

void ledBlink(int count, int velocity) {
  for (int i = 0; i < count; i++) {
    ledOn();
    delay(velocity);
    ledOff();
    delay(velocity);
  }
}
