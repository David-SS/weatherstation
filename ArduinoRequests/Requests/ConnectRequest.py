from ArduinoRequests.AbstractRequest import AbstractRequest
from Tools.ArduinoConnection import ArduinoConnection


class ConnectRequest(AbstractRequest):
    name = "Connect Arduino"
    params_description = "[port_string]"

    def run(self, params):
        arduino_connection = ArduinoConnection()

        if not arduino_connection.is_connected():
            # Check for params to setup this request
            if len(params) == 0:
                arduino_connection.set_default_port_keyword()
            else:
                arduino_connection.set_port_keyword(params[0])

            result = arduino_connection.connect_arduino()
            if result:
                return "Executed " + self.name
            else:
                return "Connection failed."
        else:
            print("Arduino is actually connected.")
            return "Arduino is actually connected."
