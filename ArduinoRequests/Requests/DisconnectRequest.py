from ArduinoRequests.AbstractRequest import AbstractRequest
from Tools.ArduinoConnection import ArduinoConnection


class DisconnectRequest(AbstractRequest):
    name = "Disconnect Arduino"
    params_description = "[]"

    def run(self, params):
        arduino_connection = ArduinoConnection()
        if arduino_connection.is_connected():
            result = arduino_connection.disconnect_arduino()
            if result:
                return "Executed " + self.name
            else:
                return "Disconnection failed."
        else:
            print("Arduino is actually not connected.")
            return "Arduino is actually not connected."
