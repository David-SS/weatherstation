from Config.Configuration import Configuration
from ArduinoRequests.AbstractRequest import AbstractRequest
from Tools.ArduinoConnection import ArduinoConnection
from Tools.ArduinoStatus import ArduinoStatus
from Tools.HumidityCalculation import HumidityCalculation
from Tools.LogWriter import LogWriter
from datetime import datetime


class ReadSensorsRequest(AbstractRequest):
    name = "Read Sensors"
    params_description = "[]"

    def run(self, params):
        arduino_connection = ArduinoConnection()
        arduino_status = ArduinoStatus()

        arduino_to_send_command = int.to_bytes(
            Configuration.read_config_value("VALUES", "PYTHON_OK_MESSAGE", "Configuration.conf"),
            1, 'big'
        )

        if arduino_connection.is_connected():
            if arduino_status.is_arduino_ready():

                # Basic first contact with arduino (send the byte command)
                arduino_status.python_to_arduino_command(arduino_to_send_command)
                json_arduino_data, operation_time = arduino_status.read_from_arduino()

                # Add additional data to the final json
                humidity_calculation = HumidityCalculation()
                json_arduino_data["abs_humidity"] = humidity_calculation.absolute_humidity_method1(
                    json_arduino_data["temperature"],
                    json_arduino_data["humidity"]
                )
                json_arduino_data["datetime"] = datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')

                # HERE ADD FOR TESTING (custom data field and data value):
                # arduino_json_data["df"] = dv <==> Example: (df = temperature // dv = 25)

                # Log read data
                log_enabled = Configuration.read_config_value("SETTINGS", "LOG_ENABLED", "Configuration.conf")
                if log_enabled:
                    lw = LogWriter()
                    lw.log_weather_entry(
                        operation_time,
                        json_arduino_data
                    )

                # Finally send the same byte command to Arduino for ends the communication
                arduino_status.python_to_arduino_command(arduino_to_send_command)

                # To return full json to the client: str(json_arduino_data)
                return ("Executed " + self.name + " command in " + str(operation_time) + " secs. JSON Data written " +
                        "in log: Datetime = " + str(json_arduino_data["datetime"]) + " // "
                        "T: " + str(json_arduino_data["temperature"]) + " // "
                        "H: " + str(json_arduino_data["humidity"]) + " // "
                        "aH: " + str(json_arduino_data["abs_humidity"]) + " // "
                        "L: " + str(json_arduino_data["light"]))

            else:
                return "Arduino seems not to be ready to execute " + self.name + " command."
        else:
            return "Connect Arduino first before executing " + self.name + " command."
