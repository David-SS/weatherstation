from Config.Configuration import Configuration
from ArduinoRequests.Requests.ConnectRequest import ConnectRequest
from ArduinoRequests.Requests.DisconnectRequest import DisconnectRequest
from ArduinoRequests.Requests.ReadSensorsRequest import ReadSensorsRequest


class RequestsList:
    __actions = {}

    @classmethod
    def init_actions(cls):
        commands = Configuration.read_config_section("COMMANDS", "Configuration.conf")
        cls.__actions[commands['CONNECT_COMMAND']] = ConnectRequest()
        cls.__actions[commands['DISCONNECT_COMMAND']] = DisconnectRequest()
        cls.__actions[commands['READ_SENSORS_COMMAND']] = ReadSensorsRequest()

    @classmethod
    def get_actions(cls):
        return cls.__actions.items()

    @classmethod
    def get_key(cls, action_value):
        # for a, b in dictionary.iteritems():  <-- Python 2.x
        for key, action in cls.__actions.items():
            if action.get_name() == action_value:
                return key
            else:
                continue
        return None

