import socket
import sys

from Config.Configuration import Configuration


""" FUNCTIONS """

def send_server_request(client_request):
    # Create a udp socket
    ip_address = Configuration.read_config_value("SETTINGS", "SERVER_IP", "ClientConfiguration.conf")
    port = Configuration.read_config_value("SETTINGS", "SERVER_PORT", "ClientConfiguration.conf")
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server_address = (ip_address, port)

    # Send request
    print('Sending request [ {!r} ]'.format(client_request))
    sent = sock.sendto(client_request.encode(), server_address)
    return sock


def wait_server_response(sock):
    print('Waiting for response...')
    try:
        data, server = sock.recvfrom(4096)
        data = data.decode('utf-8')
        print("Received data: {}".format(data))

    except ConnectionResetError:
        print("There were problems in the server side, check it and try again...")

    print('Closing socket')
    sock.close()


""" MAIN PROGRAM """

# Get the request
arguments = sys.argv[1:]
request = ""
for argument in arguments:
    request += argument + " "
request = request.strip()

# Empty request = show commands (help command)
if len(request) == 0:
    help_command = Configuration.read_config_value("CLIENT_COMMANDS", "HELP_COMMAND", "ClientConfiguration.conf")
    request = help_command

# Send the request and wait for response
sock = send_server_request(request)
wait_server_response(sock)