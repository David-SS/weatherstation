import socket
from threading import Thread
from Config.Configuration import Configuration


""" FUNCTIONS """

def send_server_request(client_request):
    # Create a udp socket
    ip_address = Configuration.read_config_value("SETTINGS", "SERVER_IP", "ClientConfiguration.conf")
    port = Configuration.read_config_value("SETTINGS", "SERVER_PORT", "ClientConfiguration.conf")
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server_address = (ip_address, port)

    # Send request
    print('Sending request [ {!r} ]'.format(client_request))
    sent = sock.sendto(client_request.encode(), server_address)
    return sock


def wait_server_response(sock):
    print('Waiting for response...')
    try:
        data, server = sock.recvfrom(4096)
        data = data.decode('utf-8')
        print("Received data: {}".format(data))

    except ConnectionResetError:
        print("There were problems in the server side, check it and try again...")

    print('Closing socket')
    sock.close()


""" MAIN PROGRAM """

# Exit and Help keyword and first message to user
exit_command = Configuration.read_config_value("CLIENT_COMMANDS", "EXIT_COMMAND", "ClientConfiguration.conf")
help_command = Configuration.read_config_value("CLIENT_COMMANDS", "HELP_COMMAND", "ClientConfiguration.conf")
print("Type '{}' to exit or '{}' if you want to get the list of commands".format(exit_command, help_command))

while True:
    request = str(input()).strip()
    sock = send_server_request(request)

    # Check for exit command (Close client)
    if request == exit_command:
        break

    # Receive response
    wait_for_response_thread = Thread(target=wait_server_response, args=(sock,))
    wait_for_response_thread.start()



