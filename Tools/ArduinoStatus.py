import json
from Config.Configuration import Configuration
from Tools.ArduinoConnection import ArduinoConnection
from time import time


class ArduinoStatus:

    """
    Arduino will only have read commands, so we do not need
    a 'ArduinoAction' class just for one read operation (port)
    """

    # Singleton instance of this class
    __instance = None

    def __new__(cls, *args, **kwargs):
        # When call EngineStatus() we assign to "self" the new object or...
        if cls.__instance is None:
            cls.__instance = super(ArduinoStatus, cls).__new__(cls)
            cls.__instance.__initialized = False
        # the existing one (Singleton)
        return cls.__instance

    def __init__(self):
        # Only set initial values when instantiated first time
        if self.__initialized:
            return
        self.__initialized = True
        self.__arduino_connection = ArduinoConnection()
        self.__arduino_ok_message = int.to_bytes(
            Configuration.read_config_value("VALUES", "ARDUINO_OK_MESSAGE", "Configuration.conf"),
            1, 'big'
        )

    def __str__(self):
        return str(id(self)) + " - " + str(self.__dict__)

    """ 
        GENERAL STATUS METHODS
    """

    # Arduino read operation
    def is_arduino_ready(self):
        arduino_message = self.__arduino_connection.get_arduino().read(1)
        if arduino_message == self.__arduino_ok_message:
            return True
        else:
            return False

    # Arduino read operation.
    # Few sensor data (Theoretically, no problems with: size, data lost, ...)
    def read_from_arduino(self):
        # Time when start read operation
        time_start = time()

        # Substring with Json part
        arduino_json_string = self.__arduino_connection.get_arduino().readline().decode('utf-8')
        arduino_json_string = arduino_json_string[arduino_json_string.index('{'):arduino_json_string.index('}') + 1]

        # Get and print Sensor data in JSON Format (server side)
        arduino_json_data = json.loads(arduino_json_string)

        # Time when ends read operation
        time_end = time()

        return arduino_json_data, (time_end - time_start)

    # Arduino write operations
    def python_to_arduino_command(self, arduino_to_send_command):
        self.__arduino_connection.get_arduino().reset_output_buffer()
        self.__arduino_connection.get_arduino().reset_input_buffer()
        self.__arduino_connection.get_arduino().write(arduino_to_send_command)
