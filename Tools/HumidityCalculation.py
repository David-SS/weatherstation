import math


class HumidityCalculation:

    STANDARD_PRESSURE = 760

    # Water saturation pressure at temperature (mmHg)
    WSP = {
        10: 9.209, 11: 9.844, 12: 10.518, 13: 11.231,
        14: 11.987, 15: 12.788, 16: 13.634, 17: 14.530,
        18: 15.477, 19: 16.477, 20: 17.535, 21: 18.650,
        22: 19.827, 23: 21.068, 24: 22.377, 25: 23.756,
        26: 25.209, 27: 26.739, 28: 28.349, 29: 30.043,
        30: 31.824, 31: 33.695, 32: 35.663, 33: 37.729,
        34: 39.898, 35: 42.175, 36: 44.563, 37: 47.067,
        38: 49.692, 39: 52.442, 40: 55.324, 41: 58.34
    }

    # Method 1: Only need sensor data
    def absolute_humidity_method1(self, humidity, temperature):
        euler = math.exp((17.67 * temperature) / (temperature + 243.5))
        return 6.112 * euler * humidity * 2.1674 / (273.15 + temperature)

    # Only for water level pressure (760mmHg)
    def absolute_humidity_method2(self, humidity, temperature):
        print("\n Calculating...")
        saturation_pressure = self.interpolate_saturation_presure(temperature)
        partial_pressure = (humidity / 100) * saturation_pressure
        print("\nSaturation Pressure: " + str(saturation_pressure))
        print("Partial Pressure: " + str(partial_pressure))
        absolute_humidity = (partial_pressure * 18) / ((self.STANDARD_PRESSURE - partial_pressure) * 29)
        print("Absolute Humidity: " + str(absolute_humidity))

    def interpolate_saturation_presure(self, temperature):
        # x = temperatures // y = pressure
        x1 = int(temperature)
        x2 = x1 + 1
        y1 = self.WSP[x1]
        y2 = self.WSP[x2]
        return (y1 * (x2 - temperature)) + (y2 * (temperature - x1)) / (x2 - x1)
