from os.path import dirname, join, getsize


class LogWriter:

    FILENAME = "WSLog.txt"

    def __init__(self):
        self.ROOT_DIR = dirname(dirname(__file__))
        self.FILEPATH = join(self.ROOT_DIR, self.FILENAME)

    def open_log(self, open_message):
        file = open(self.FILEPATH, "a")
        if getsize(self.FILEPATH) <= 0:
            file.write(open_message)
        return file

    ''' FULL WEATHER STATION LOG '''

    def log_weather_entry(self, time, json_arduino_data):
        file = self.open_log(" WEATHER STATION LOG\n=====================\n\n")
        file.write("[{0}] -- [T:{1} || H:{2} || aH:{3} || L:{4}] -- [{5}]\n".format(
            json_arduino_data["datetime"],
            json_arduino_data["temperature"],
            json_arduino_data["humidity"],
            json_arduino_data["abs_humidity"],
            json_arduino_data["light"],
            time,
        ))
        file.close()

    '''
        ISOLATED LOG VARIABLES IF NEEDED:
         * TEMPERATURE
         * HUMIDITY
         * ABSOLUTE HUMIDITY
         * LIGHT
    '''

    def log_temperature(self, time, temperature):
        file = open(self.FILEPATH, "a")
        file.write("{0} ---- {1} \n".format(
            time,
            temperature
        ))
        file.close()

    def log_humidity(self, time, humidity):
        file = open(self.FILEPATH, "a")
        file.write("{0} ---- {1} \n".format(
            time,
            humidity
        ))
        file.close()

    def log_abs_humidity(self, time, abs_humidity):
        file = open(self.FILEPATH, "a")
        file.write("{0} ---- {1} \n".format(
            time,
            abs_humidity
        ))
        file.close()

    def log_light(self, time, light):
        file = open(self.FILEPATH, "a")
        file.write("{0} ---- {1} \n".format(
            time,
            light
        ))
        file.close()
