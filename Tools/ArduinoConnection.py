import re
import serial.tools.list_ports
from Config.Configuration import Configuration
from Tools.HumidityCalculation import HumidityCalculation


class ArduinoConnection:
    # Singleton instance of this class
    __instance = None
    __connected = False

    def __new__(cls, *args, **kwargs):
        # When call EngineStatus() we assign to "self" the new object or...
        if cls.__instance is None:
            cls.__instance = super(ArduinoConnection, cls).__new__(cls)
            cls.__instance.__initialized = False
        # the existing one (Singleton)
        return cls.__instance

    def __init__(self):
        # Only set initial values when instantiated first time
        if self.__initialized:
            return
        self.__initialized = True
        self.__arduino_port = 'None'
        self.__default_port_keyword = Configuration.read_config_value("SETTINGS", "PORT_KEYWORD", "Configuration.conf")
        self.__port_keyword = self.__default_port_keyword
        self.__arduino = None  # No connection yet

    def __str__(self):
        return str(id(self)) + " - " + str(self.__dict__)

    """ 
        CONNECTION METHODS
    """

    def get_port(self):
        if self.__arduino_port.__eq__('None'):
            self.find_port()
        return self.__arduino_port

    def find_port(self):
        available_ports = serial.tools.list_ports.comports()
        num_of_ports = len(available_ports)
        for i in range(0, num_of_ports):
            current_port = str(available_ports[i])
            if self.__port_keyword in current_port:
                self.__arduino_port = re.search('COM[0-9]{1,2}', current_port).group()
                break

    def get_arduino(self):
        return self.__arduino

    def is_connected(self):
        return self.__connected

    def set_port_keyword(self, keyword):
        self.__port_keyword = keyword

    def set_default_port_keyword(self):
        self.__port_keyword = self.__default_port_keyword

    def clean_connection_data(self):
        self.__arduino_port = 'None'

    def connect_arduino(self):
        try:
            self.__arduino = serial.Serial(
                self.get_port(),
                9600
            )
            self.__connected = True
            # sleep(2) # Optional
            return True
        except serial.SerialException as ex:
            print("Failed to connect to Arduino")
            print(ex)
            return False

    def disconnect_arduino(self):
        try:
            self.__arduino.close()
            self.clean_connection_data()
            self.__connected = False
            # sleep(2) # Optional
            return True
        except Exception as ex:
            print("Failed to disconnect")
            print(ex)
            return False
